rm commodorella_eduskuntaan.d64
c1541 -format "eduskuntaan 2023,0" d64 commodorella_eduskuntaan.d64
c1541 -attach commodorella_eduskuntaan.d64 -write piraatti_intro.prg piraattipuolue
c1541 -attach commodorella_eduskuntaan.d64 -write helsinki.prg helsinki
c1541 -attach commodorella_eduskuntaan.d64 -write helsinki-uusimaa.prg helsinki-uusimaa
c1541 -attach commodorella_eduskuntaan.d64 -write muu-maa.prg muu-maa
