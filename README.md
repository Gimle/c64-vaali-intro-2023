# Commodorella Eduskuntaan 2023

Commodore 64:llä toteutettu vaalimainos vanhaan kuusnepa demoscene tyyliin
Musiikki toteutettu SIDFactory II

### Levyimagen lataaminen

1. Valitse tiedosto `commodorella_eduskuntaan.d64` ja lataa.
2. Kirjoita tikulle tai oikealle 1541 levykkeelle.

### Kääntäminen

1. Lataa [Turbo Rascal Syntax Error](https://lemonspawn.com/turbo-rascal-syntax-error-expected-but-begin/) retro-kehitysympäristö koneellesi (Win/Mac/Linux)
2. Asenna [VICE](https://vice-emu.sourceforge.io/) tai muu Commodore 64 emulaattori
3. Aseta TRSE emulaattori-asetukset
4. Avaa piraatti_intro.trse Turbo Rascal Syntax Errorissa
5. Avaa `piraaatti_intro.ras` ja aja painalla ctrl-r tai cmd-r

### Oman version tekeminen

1. Valmistele 2 kpl 320x200 resoluution png-kuvatiedostoa, joissa kukin ehdokkaan naaama on 104x96 pikselin kokoisena. Oikeaan reunaan ja alareunaan pitäisi jäädä tyhjät 8 pikselin palkit jos koot ovat oikein. Katso esimerkki `naamat-esimerkki.png` tiedostosta.
2. Avaa `resources/images/piraattinaamat-1.flf` sekä `resources/images/piraattinaamat-2.flf`
3. Käytä import image toimintoa tuodaksesi luomasi kuvatiedostot commodore 64-muotoon
4. Avaa `piraatti_intro.ras` ja mene riville 220 josta alkaa muuttuja `naamatNames : string = (`
5. Täytä ehdokkaiden nimet ja vaalipiirit
6. Mene riville 235 josta alkaa muuttuja `naamatNumbers : string = (`
7. Täytä ehdokasnumerot, ja käytä _tasan_ kolme merkkiä jokaiseen, muuten intro ei toimi!
8. Käännä ja aja tiedosto, tarkista että toimii
9. Exporttaa haluamaasi muotoon tai nauhoita video!

### Näyttäminen vaalikojulla fullscreeninä suoraan emulaattorista

1. Aja build tai run TRSEllä (ctrl-b / cmd-b)
2. Etsi projektin juuresta `piraatti_intro.prg`
3. Avaa VICE ja c64 käynnistyä
4. Jos `Preferences` valikossa on valittuna `Show menu/status in fullscreen` ota se pois päältä
5. Raahaa `piraatti_intro.prg` emulaattorin ikkunan päälle
6. Tuplanapauta emulaattorin ikkunaa siirtyäksesi fullscreeniin
